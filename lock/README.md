# LockInterceptor

# 介绍
- 给指定接口添加同步锁
- 给指定代码块添加同步锁



# 使用说明

## 整个接口加同步锁
- 需要添加同步锁的接口，在对应接口的***接口选项***里添加标记
    > lock : -1 (超时时间，为负则一直等待) 
## 单独代码块加锁
- 在需要加锁的代码前调用lock()函数加锁
- 在需要加锁的代码后调用unlock()函数释放

